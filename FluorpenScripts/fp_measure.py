#!/usr/bin/env python
"""
=====================================
fp_measure
=====================================


Example:
    $ ./fp_measure.py [PORT] [PROTOCOL] [f_pulse] [F_pulse]
    $ ./fp_measure.py /dev/ttyUSB1 0 80 80


:Author: Michal Vozda / vozda@psi.cz
:Date: 2018-03-08
"""

import sys
from serial import Serial
from time import sleep
from datetime import datetime
from os import path

PEN_SETTING_ID_PARSERS = {1: 'switch_parser', 2: 'value_parser', 3: 'ptime_parser', 4: 'choice_parser'}
UART_BAUDRATE = 19200


class Uart(Serial):
    """

    """
    def __init__(self, port, baudrate, timeout):
        # Tries to connect to UART
        Serial.__init__(self, port=port, baudrate=baudrate, timeout=timeout, write_timeout=timeout)
        return

    def uart_send(self, CMD):
        """
        Method used for sending commands into device
        :param CMD: Command which is sent into device (including data)
        :return:
        """
        # Clears buffers
        self.flushInput()
        self.flushOutput()

        # Sends the data
        self.write(CMD)
        return 0

    def uart_send_read(self, CMD, terminator='\x00'):
        """
        uart_send command including data and read the answer of the device
        :param CMD: Command
        :param terminator: String which terminates received data
        :return: Read data
        """
        # Clears buffers
        self.flushOutput()
        self.flushInput()

        # Sent command
        self.write(CMD)

        # Read data terminating of the defined string or until timeout
        data_raw = self.read_until(terminator=terminator)
        return data_raw

    def dev_ping(self):
        """
        uart_send Who command and reads the answer of the device
        :return: Name of the device
        """
        try:
            buf = self.uart_send_read("WH", '\x00')
        except:
            buf = False
        return buf


class Device(object):
    """ This class defines interface and tools to control and measure with Pen devices

    There are used two interfaces for device connection wired UART and wireless XBEE
    """

    def __init__(self, connection, dev_info):
        self.device = DeviceParam(dev_info)
        self.serial = connection
        self.setting = DeviceSetting(self.serial)
        self.measurement = Measurement(self.serial)
        return

    def protocol_measure(self, protocol):
        """ Method for measuring protocol

        Measures the protocol e.g. 'Ft', 'OJIP', ...
        :param protocol: Measured protocol, the format of the command is defined in FW of the device
        :return:
        """
        if self.wait_to_finish_measurement():  # Waits while the device is busy
            return 1

        try:
            self.measurement.do_protocol_measure(protocol)  # protocol_measure protocol
            sleep(1)
        except:
            print('WARNING: Can not measure protocol ' + protocol + ' for device: ' + self.device.name)
            return 1
        return 0

    def protocol_set(self, cmd, val):
        """
        Sets parameter given by the 'cmd' to value 'val' e.g. 'AI' to value 90 or 'SN' to value 0
        :param cmd: Command
        :param val: Setting value
        :return:
        """
        if self.wait_to_finish_measurement():  # Waits while the device is busy
            return 1
        try:
            self.measurement.protocol_setting_set(cmd, val)
            sleep(1)
        except:
            print('WARNING: Can not set protocol ' + cmd + ' for the device: ' + self.device.name)
            return 1
        return 0

    def time_set(self):
        """
        Main method for updating time in the device to system time of the computer (BeagleBone)
        :return:
        """
        if self.wait_to_finish_measurement():  # Waits while the device is busy
            return 1

        try:
            self.setting.do_set_time()
        except:
            print('WARNING: Can not set time for device: ' + self.device.name)
            return 1
        return 0

    def sound_set(self, status):
        if self.wait_to_finish_measurement():  # Waits while the device is busy
            return 1

        try:
            self.setting.do_set_sound(status)
        except:
            print('WARNING: Cannot set sound for device: ' + self.device.name)
            return 1

        return 0

    def data_download(self):
        """
        Main function for downloading data from the device
        *   Erases data after success download
        :return:
        """
        # sleep(1)
        # if self.wait_to_finish_measurement():  # Waits while the device is busy
        #    return 1
        sleep(1)

        try:
            self.setting.do_read_memory(self.device.name)
            sleep(1)
        except:
            print('WARNING: Can not download data from FP: ' + self.device.name)
            return 1

        self.setting.do_erase_memory()  # Memory of the device is erased after download
        sleep(1)
        return 0

    def wait_to_finish_measurement(self, timeout=5, timeout_busy=0):
        """
        Computer waits until the device is ready for communication
            While the device is busy the answer for ping is name-Busy e.g. FluorPen-Busy
        :return:
        """
        n = 0
        status = 1
        period_in_busy = 4
        err_count = 0
        timeout_busy /= period_in_busy

        if timeout_busy > 1:
            while n < timeout_busy:
                sleep(0.1)
                status = self.is_busy()
                if status == 0:
                    break
                elif status == 1:
                    err_count += 1
                if err_count > 10:
                    break

                sleep(period_in_busy)
                n += 1
        else:
            while n < timeout:
                sleep(0.1)
                status = self.is_busy()
                if not status:
                    break
                sleep(0.9)
                n += 1

        return status

    def is_busy(self):
        """
        Testing of the device answer for ping, if the last word is Busy - the device is still measuring
        :return: 0 if the device is Ready, 2 when is Busy and 1 for communication Error
        """
        self.serial.flushInput()
        self.serial.flushOutput()
        rdata = self.serial.uart_send_read("WH")
        # print(rdata)
        if rdata[-5:-1] == "Busy":
            return 2
        elif (rdata[0:2] == 'OK') or not bool(rdata):
            return 1
        else:
            return 0


class DeviceSetting:
    """
    Class with methods for setting of the device
    """

    def __init__(self, device_connection):
        self.serial = device_connection  # Needs access to serial port
        return

    def do_set_sound(self, status):
        """
        Setting of the buzzer
        :param status:
        :return:
        """
        if status == 0:
            self.serial.uart_send("DSSN0\x00")
        else:
            self.serial.uart_send("DSSN1\x00")

        sleep(2)  # It takes time to set it in FP
        return 0

    def do_set_time(self):
        """
        Updates time of the device according system time of the computer
        :return:
        """

        now = datetime.now()  # System time of the computer

        # Reformat the time into proper format
        buf = str(now.year - 2000).zfill(2) + str(now.month).zfill(2) + str(now.day).zfill(2) + str(now.hour).zfill(
            2) + str(now.minute).zfill(2) + str(now.second).zfill(2)

        # Setting time
        self.serial.uart_send("DSTM" + buf + '\x00')

        sleep(2)  # It takes time to set it in FluorPen
        return 0

    def do_read_memory(self, device_name="Fluorpen"):
        """
        Reads memory of the device and save it into file in the Data folder defined by DATA_FOLDER_PATH.
            Data file are stored in separate folders defined by names of the device
        :return:
        """
        last_byte = 0

        # Open serial interface if it is not
        if not self.serial.isOpen():
            self.serial.open()
            sleep(2)
            close_after = 1
        else:
            close_after = 0

        # Ping the device
        if self.serial.dev_ping() == 0:
            print("WARNING: Device does not response")
            return 1

        ###############################################################################################################
        # Needs some improvements after update of the FW of the Fluorpen
        ###############################################################################################################
        while True:  # !!!!!!!!!!!!!!!will be removed later!!!!!!!!!!!!!!!

            # Testing of the device response
            att = 0
            att_const = 5  # Number of attempts
            while (self.serial.dev_ping() == 0) and att < att_const:
                sleep(0.5)
                att += 1
                if att == att_const:
                    print("WARNING: Device does not response")
                    return 1

            # Testing of the last byte format
            att = 0
            while att < att_const:
                last_byte = self.serial.uart_send_read("ML", '\x00')  # Reads the last byte in the memory
                int_len = int(last_byte[:-1])  # Convert string length into number
                if int_len >= 0:
                    break
                elif att == att_const:
                    print('WARNING: Device does not response to ML command')
                    return 1
                else:
                    att += 1

            # Creates header in the datafile
            len_hex = self._int_to_rev_hex(int_len)

            # Sends command for reading data from the device
            self.serial.uart_send("MR0:" + last_byte)
            n = 0  # Controls number of attempts for reading data

            # Reads data per block
            while int_len > 0:
                n += 1
                if int_len > 256:
                    data = self.serial.read(256 + 1)
                else:
                    data = self.serial.read(int_len + 1)

                # Empty data breaks the loop
                l = len(data)
                if l <= 1:
                    print('WARNING: Empty byte from FP')
                    break

                # Last byte in the data is checksum
                checksum = ord(data[-1])
                # Others are data
                data = data[:-1]

                # Checksum of the received data
                data_sum = 0
                for i in range(0, l - 1):
                    data_sum += ord(data[i])

                # Converts into char
                data_sum = (256 - (data_sum % 256)) % 256

                # Comparison of the checksum
                if data_sum == checksum:
                    self.serial.write('OK')
                    int_len -= (l - 1)
                    #print(data)
                else:
                    self.serial.write('ER')
                    print('WARNING: Incorrect checksum!')
                    break

            # Breaks the loop if all data are read
            if int_len <= 0:
                break

        self.serial.flushInput()
        self.serial.flushOutput()
        if close_after:
            self.serial.close()

        return data

    def do_erase_memory(self):
        """
        Methods erasing the memory of the device
        :return:
        """
        self.serial.uart_send("ME\x00")
        sleep(2)
        return

    @staticmethod
    def _int_to_rev_hex(int_len):
        """
        Converts the int into hex in the reverse format according the FluorPen data file format
        :param int_len:
        :return:
        """
        len_hex = "%0.8X" % int_len
        len_hex = len_hex.decode('hex')[::-1]
        return len_hex

    @staticmethod
    def _rev_hex_to_int(len_hex):
        """
        Converts hex format into int
        :param len_hex:
        :return:
        """
        int_len = int(len_hex[::-1].encode('hex'), 16)
        return int_len


class Measurement:
    """
    This class defines methods and attributes for measurement purpose
    """

    def __init__(self, connection):
        self.list_of_protocols = dict()
        self.list_of_settings = dict()
        self.serial = connection

    def protocol_get(self):
        """
        Gets the information about measurements which the device allows
            The names of protocols and its commands are stored in the attribute listOfProtocols
        """
        n = 0
        while n < 10:
            # Reads protocols from the device
            protocols = self.serial.uart_send_read("GP", '\x00\x00')

            # print(protocols)

            # Tries to parse protocols
            try:
                self.list_of_protocols = self.protocol_parse(protocols)
                break
            except:
                sleep(2)
                n += 1

        return 1 if n == 10 else 0

    def protocols_setting_get(self):
        """
        Gets the current setting of the individual protocols and measurements
        :return:
        """
        while True:
            # Reads setting from the device
            setting = self.serial.uart_send_read("DI", terminator='\x00\x00\x00\x00')

            # Tries to parse received data
            try:
                self.list_of_settings = self.setting_parse(setting)
                break
            except:
                sleep(2)
        return 0

    def do_protocol_measure(self, protocol):
        """
        Measures the input protocol
        :param protocol:
        :return:
        """
        # Tests if there is generated list of protocols which are allowed in the device
        if not bool(self.list_of_protocols):
            self.protocol_get()

        # Tests if the input protocol is in the list of protocols, if not reads protocols again
        if not protocol in self.list_of_protocols.keys():
            sleep(2)
            self.protocol_get()
            print('WARNING: ' + protocol + ' is not in the list of protocols')

        # Tries to measure protocol
        try:
            self.serial.uart_send("RP" + self.list_of_protocols.get(protocol) + '\x00')
        except:
            print('WARNING: Measurement of ' + protocol + ' failed')
        return 0

    def protocol_setting_set(self, param, value):
        """
        Setting of the measurement
        :param param: Setting parameter
        :param value: Setting value of the parameter
        :return:
        """
        # Test if there is a list of settings. If not, get one
        if not bool(self.list_of_settings):
            self.protocols_setting_get()

        # Testing if the parameter is in the list of settings. If not reads it again
        if not param in self.list_of_settings.keys():
            sleep(2)
            self.protocols_setting_get()
            print('WARNING: ' + param + ' is not in the list of settings')

        # Tries to set parameter
        try:
            self.serial.uart_send("DS" + self.list_of_settings.get(param).cmd + str(value) + '\x00')
        except:
            print('WARNING: Setting of ' + param + ' failed')

        ###############################################################################################################
        #   Range control can be added in future
        ###############################################################################################################
        return 0

    def setting_parse(self, buf):
        """ Parsing settings

        The input string includes setting of the implemented protocols. Each protocol message includes header
        with type and length of message. The core of the message is parsed with defined parser according ID.

        :param buf: String
        :return: Dictionary with parsed information
        """
        i = 0

        # Dictionary for setting
        setting_dict = dict()

        # Testing of the validity of the message
        if buf == '':
            return 1

        while buf[0] is '\x00':
            del (buf[0])

        if len(buf) is 0:
            return 1

        # Split message to individual settings
        while i < len(buf) - 1:
            # This setting includes ID
            id_of_parser = ord(buf[i])
            i += 1

            # Length of the setting message
            msg_len = ord(buf[i])

            # Core of the message
            i += 1
            msg = buf[i:i + msg_len]
            i += msg_len

            # Get proper parser for this kind of message
            func = globals().get(PEN_SETTING_ID_PARSERS[id_of_parser])

            # Calling the parser
            try:
                params = func(msg)
            except:
                print('WARNING: Error in parsing Setting msg: ' + msg)

            # Result is add to dict
            setting_dict[params.cmd] = params

        return setting_dict

    @staticmethod
    def protocol_parse(buf):
        """ Parsing supported protocols

        :param buf: String
        :return: Dictionary with parsed information
        """
        protocols_dict = dict()
        buf = buf.split('\x00')

        for i in buf:
            if i is not '':
                protocols_dict[i[1:]] = i[0]
        return protocols_dict


class DeviceParam:
    """
    """
    def __init__(self, kwargs):
        self.name = ""
        for i in kwargs:
            setattr(self, i, kwargs.get(i))
        return


# Here are defined structures for each type of parser
class PenSettingSwitch(object):
    def __init__(self):
        self.description = ''
        self.cmd = ''
        self.choices = []
        self.value = ''


class PenSettingValue(object):
    def __init__(self):
        self.description = ''
        self.cmd = ''
        self.value = ''
        self.min = ''
        self.max = ''
        self.units = ''


class PenSettingTime(object):
    def __init__(self):
        self.cmd = ''
        self.year = ''
        self.month = ''
        self.day = ''
        self.hour = ''
        self.minute = ''
        self.second = ''


class PenSettingChoice(object):
    def __init__(self):
        self.description = ''
        self.value = ''
        self.cmd = ''
        self.description = ''
        self.count = ''
        self.choices = []


class MeasuredData(object):
    def __init__(self):
        self.datetime = None
        self.protocol = None
        self.f0_flash = None
        self.f0_background = None
        self.fm_flash = None
        self.fm_background = None


def switch_parser(buf):
    """ Parser for switch format

    e.g. Sound can be on or off, the possible states are notify by Pen device as msg: \x00SNSound\x00On\x00Off\x00\x00

    value: Current state of the setting e.g. Sound is off = \x00
    cmd: Command which is used to change the value, it is always 2 characters e.g. cmd to switch sound on is SN
    description: Usually name of the parameter which is parsing e.g. we can change Sound :)
    choices 0, choices 1: The parameters which can be set e.g. Sound can be on or off

    :param buf: String
    :return: Structure defined in PenSettingSwitch class
    """

    # Parsing current value it is always 1B binary number
    i = 0
    params = PenSettingSwitch()

    params.value = (ord(buf[i]))
    i += 1

    # Parsing CMD it is always 2 chars
    params.cmd = (buf[i:i + 2])
    i += 2

    # Parsing description it is string terminated by binary 0
    l = 0
    while ord(buf[i + l]) is not 0:
        l += 1
    params.description = (buf[i:i + l])
    i += l + 1

    # Parsing two parameters which is switching between, each of them is terminated by binary 0
    for rep in range(2):
        l = 0
        while ord(buf[i + l]) is not 0:
            l += 1
        params.choices.append(buf[i:i + l])
        i += l + 1

        # Returns structure
    return params


def value_parser(buf):
    """ Parser for value format

    e.g. "\x00\x60AIActinic light intensity\x00%\x00\x05\x00\x64"
         Parameter is set to 16b value 0x0060, it is possible to change it by AI command,
         it is called Actinic light intensity, units are %, minimum value is 0x05, maximum value is 0x64

    :param buf: String
    :return: Structure defined in PenSettingValue class
    """
    # Parsing current value it is 16b
    i = 0
    params = PenSettingValue()

    # High byte
    high = ord(buf[i])
    i += 1

    # Low byte
    low = ord(buf[i])
    i += 1

    # 16b value
    params.value = (high << 8) | low

    # Parsing command, it is always 2 characters
    params.cmd = buf[i:i + 2]
    i += 2

    # Parsing description, it is terminated by binary 0
    l = 0
    while ord(buf[i + l]) is not 0:
        l += 1
    params.description = buf[i:i + l]
    i += l + 1

    # Parsing value limits, presented as 2 16b numbers
    l = 0
    while ord(buf[i + l]) is not 0:
        l += 1
    params.units = buf[i:i + l]
    i += l + 1

    # Minimum value
    high = ord(buf[i])
    i += 1
    low = ord(buf[i])
    i += 1
    params.min = high << 8 | low

    # Maximum value
    high = ord(buf[i])
    i += 1
    low = ord(buf[i])
    i += 1
    params.max = high << 8 | low
    return params


def ptime_parser(buf):
    """ Parsing time information

    e.g. "TM\x11\x03\x02\x0C\x0B\x0A"
         The Pen device responses in ptime format. TM says that it can be changed by command TM,
         current year is \x11 it means 2017, month 3, day 2, it is 12:11:10

    >>> pt = ptime_parser(["T", "M", 0x11, 0x03, 0x02, 0x0c, 0x0b, 0x0a])
    >>> print(pt.year)
    17
    >>> print(pt.month)
    3
    >>> print(pt.day)
    2
    >>> print(pt.hour)
    12
    >>> print(pt.minute)
    11
    >>> print(pt.second)
    10

    :param buf: String
    :return: Structure defined in PenSettingTime class
    """
    params = PenSettingTime()
    params.cmd = buf[0:2]
    params.year = buf[2]
    params.month = buf[3]
    params.day = buf[4]
    params.hour = buf[5]
    params.minute = buf[6]
    params.second = buf[7]
    return params


def choice_parser(buf):
    """ Parser for selection of parameter from a list of parameters?

    e.g.

    :param buf: String
    :return: Structure defined in PenSettingSwitch class
    """
    # Parsing value
    i = 0
    params = PenSettingChoice()
    params.value = buf[i]

    # Parsing command
    i += 1
    params.cmd = buf[i:i + 2]
    i += 2

    # Parsing Description, it is always terminated by 0
    l = 0
    while ord(buf[i + l]) is not 0:
        l += 1
    params.description = buf[i:i + l]
    i += l + 1

    # Number of parameters
    params.count = buf[i]
    i += 1

    # Parsing parameters, terminated by 0
    for k in range(ord(params.count)):

        l = 0
        while ord(buf[i + l]) is not 0:
            l += 1
        params.choices.append(buf[i:i + l])
        i += l + 1
    return params


def data_parse(data):

    # Conversion string into numbers
    data = [ord(d) for d in data]

    n = 0

    measurement = []

    while n < len(data):
        ID = data[n]
        n += 1

        measurement_buff = MeasuredData()

        if ID == 0x01:
            #time
            second = data[n+1] & 0x3f
            minute = (data[n] & 0x0f) << 2 | data[n+1] >> 6
            month = data[n] >> 4
            hour = data[n+3] & 0x1f
            day = (data[n+2] & 0x03) << 3 | data[n+3] >> 5
            year = (data[n+2] >> 2) + 2000

            measurement_buff.datetime = datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second)

            n += 4

        ID = data[n]
        measurement_buff.protocol = ID
        n += 1

        if ID == 0x0b:
            #Ft
            measurement_buff.f0_flash = data[n] << 24 | data[n+1] << 16 | data[n+2] << 8 | data[n+3]
            measurement_buff.f0_background = data[n+4] << 24 | data[n+5] << 16 | data[n+6] << 8 | data[n+7]

            n += 8

        elif ID == 0x0c:
            #Qy
            measurement_buff.f0_flash = data[n] << 24 | data[n+1] << 16 | data[n+2] << 8 | data[n+3]
            measurement_buff.f0_background = data[n+4] << 24 | data[n+5] << 16 | data[n+6] << 8 | data[n+7]
            measurement_buff.fm_flash = data[n+8] << 24 | data[n+9] << 16 | data[n+10] << 8 | data[n+11]
            measurement_buff.fm_background = data[n+12] << 24 | data[n+13] << 16 | data[n+14] << 8 | data[n+15]
            n += 16

        measurement.append(measurement_buff)

    return measurement


if __name__ == "__main__":

    port = str(sys.argv[1])

    if not path.exists(port):
        print("ERROR: %s does not exist" % port)
        sys.exit()

    try:
        measure = int(sys.argv[2])
    except:
        print("ERROR: Input parameter measurement is not a number")
        sys.exit()

    try:
        f_pulse = int(sys.argv[3])
    except:
        print("ERROR: f_pulse is not a number")
        sys.exit()

    try:
        F_pulse = int(sys.argv[4])
    except:
        print("ERROR: F_pulse is not a number")
        sys.exit()

    serial = Uart(port, UART_BAUDRATE, 5)

    try:
        serial.open()
    except:
        pass

    Dev = Device(serial, {'name': 'Fluorpen'})
    Dev.setting.do_erase_memory()

    if 10 < f_pulse < 100:
        Dev.measurement.protocol_setting_set('FI', f_pulse)
    else:
        print("ERROR: f_pulse is not in the range from 10 to 100")
        sys.exit()

    if 10 < F_pulse < 100:
        Dev.measurement.protocol_setting_set('SI', F_pulse)
    else:
        print("ERROR: F_pulse is not in the range from 10 to 100")
        sys.exit()

    if measure == 0:
        Dev.measurement.do_protocol_measure("Ft")

    elif measure == 1:
        Dev.measurement.do_protocol_measure("Qy")

    else:
        print("ERROR: Unknown protocol 0-Ft, 1-Qy")
        sys.exit()

    Dev.wait_to_finish_measurement()

    data = Dev.setting.do_read_memory()
    measurement = data_parse(data)

    for m in measurement:
        if m.protocol == 0x0b:
            # Ft
            result = m.f0_flash - m.f0_background

        elif m.protocol == 0x0c:
            # Qy
            f0 = m.f0_flash - m.f0_background
            fm = m.fm_flash - m.fm_background

            if fm > f0:
                result = float(fm-f0)/fm
                result = str("%.3f" % result)
            else:
                result = 0.0

    serial.close()

    print(result)
